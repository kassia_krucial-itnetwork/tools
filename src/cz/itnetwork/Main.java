package cz.itnetwork;

public class Main {

    public static void main(String[] args) {
        Crate crate = new Crate(10000);

        Hammer kladivko = new Hammer(1000, "Klad�vko", false);
        Hammer velkeKladivo = new Hammer(3000, "Velk� kladivo", true);
        Hammer bouraciKladivo = new Hammer(4000, "Bourac� kladivo", true);
        Screwdriver sroubovakPhilips = new Screwdriver(500, "�roubov�k Philips");
        Saw zrezivelaPila = new Saw(2000, "Zreziv�l� pila");
        ElectricScrewdriver bosh = new ElectricScrewdriver(1000,
                "Elektrick� �roubov�k Bosh", 4000);

        System.out.println(crate);

        crate.addTool(kladivko);
        crate.addTool(velkeKladivo);
        crate.addTool(bouraciKladivo);
        crate.addTool(sroubovakPhilips);
        crate.addTool(bosh);

        System.out.println(crate);
    }
}
