package cz.itnetwork;

public abstract class Tool {
    private int weight;
    private String name;

    public Tool(int weight, String name) {
        this.weight = weight;
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public String getName() {
        return name;
    }

    public abstract String work();
}
