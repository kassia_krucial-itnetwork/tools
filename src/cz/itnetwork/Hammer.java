package cz.itnetwork;

public class Hammer extends Tool {
    private boolean isTwoHanded;

    public Hammer(int weight, String name, boolean isTwoHanded) {
        super(weight, name);
        this.isTwoHanded = isTwoHanded;
    }

    public boolean isTwoHanded() {
        return isTwoHanded;
    }

    @Override
    public String work() {
        return "Zatloukám";
    }
}
