package cz.itnetwork;

import java.util.ArrayList;

public class Crate {
    private int maxWeightCapacity;
    private int weightOfTools;
    private ArrayList<Tool> tools;

    public Crate(int maxWeightCapacity) {
        this.maxWeightCapacity = maxWeightCapacity;
        weightOfTools = 0;
        tools = new ArrayList<Tool>();
    }

    public void addTool(Tool tool) {
        if ((tool.getWeight() + weightOfTools) <= maxWeightCapacity) {
            weightOfTools = weightOfTools + tool.getWeight();
            tools.add(tool);
        }
    }

    public void removeTool(Tool tool) {
        if (tools.contains(tool)) {
            tools.remove(tool);
            weightOfTools = weightOfTools - tool.getWeight();
            System.out.println("Vynd�v�m n�stroj: " + tool.getName());
        } else {
            System.out.println("Tento n�stroj v bedn� nen�");
        }
    }

    public String getWeightOfTwoHandedHammers() {
        int weightOfTwoHandedHammers = 0;
        for (Tool tool : tools) {
            if (tool instanceof Hammer && ((Hammer)tool).isTwoHanded()) {
                weightOfTwoHandedHammers = weightOfTwoHandedHammers + tool.getWeight();
            }
        }
        return "V�ha obouru�n�ch kladiv je " + weightOfTwoHandedHammers + " gram�";
    }

    public String toolWork() {
        String text = "";
        for (Tool tool : tools) {
            text = text + tool.work() + "\n";
        }
        return text;
    }

    @Override
    public String toString() {
        if (!tools.isEmpty()) {
            String text = "V bedn� je: ";
            while (tools.iterator().hasNext()) {
                if (!tools.iterator().equals(tools.get(tools.size() - 1))) {
                    text = text + tools.iterator().next().getName() + ", ";
                } else {
                    text = text + tools.iterator().next();
                }
            }
            return text;
        } else {
            return "Bedna je pr�zdn�.";
        }
    }
}
