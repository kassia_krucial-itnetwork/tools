package cz.itnetwork;

public class ElectricScrewdriver extends Screwdriver {
    private int batteryCapacity;

    public ElectricScrewdriver(int weight, String name, int batteryCapacity) {
        super(weight, name);
        this.batteryCapacity = batteryCapacity;
    }
}
